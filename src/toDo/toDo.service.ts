import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';

import { ToDo } from './toDo.model';

@Injectable()
export class ToDoService {
  constructor(
    @InjectModel('ToDo') private readonly toDoModel: Model<ToDo>,
  ) {}

  async createToDo(title: string, description: string, type: number) {
    const newToDo = new this.toDoModel({
      title,
      description,
      type,
    });
    const result = await newToDo.save();
    return result as object;
  }

  async getToDoList() {
    const toDo = await this.toDoModel.find().exec();
    return toDo.map(item => ({
      id: item.id,
      title: item.title,
      description: item.description,
      type: item.type,
    }));
  }

  async getSingleToDo(toDoId: string) {
    const item = await this.toDoModel.findById(toDoId).exec();
    if ( !item )  throw new NotFoundException('Could not find element.');
    return {
      id: item.id,
      title: item.title,
      description: item.description,
      type: item.type,
    };
  }

  async updateToDo(
    toDoId: string,
    title: string,
    desc: string,
    type: number,
  ) {
    const obj = {
      title: undefined,
      description: undefined,
      type: undefined,
    };
    if ( title ) obj.title = title;
    if ( desc ) obj.description = desc;
    if ( type ) obj.type = type;

    await this.toDoModel.findOneAndUpdate( { _id : toDoId }, { $set : obj } );
    return {
      title: obj.title,
      description: obj.description,
      type: obj.type,
    };
  }
}
