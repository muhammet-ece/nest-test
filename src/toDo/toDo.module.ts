import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';

import { ToDoController } from './toDo.controller';
import { ToDoService } from './toDo.service';
import { ToDoSchema } from './toDo.model';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'ToDo', schema: ToDoSchema }]),
  ],
  controllers: [ToDoController],
  providers: [ToDoService],
})
export class ToDoModule {}
