import {
  Controller,
  Post,
  Body,
  Get,
  Param,
  Patch,
} from '@nestjs/common';

import { ToDoService } from './toDo.service';

@Controller('toDo')
export class ToDoController {
  constructor(private readonly toDoService: ToDoService) {}

  @Post()
  async addProduct(
    @Body('title') itemTitle: string,
    @Body('description') itemDesc: string,
    @Body('type') itemType: number,
  ) {
    const generatedId = await this.toDoService.createToDo(
      itemTitle,
      itemDesc,
      itemType,
    );
    return { id: generatedId };
  }

  @Get()
  async getAllToDoList() {
    const items = await this.toDoService.getToDoList();
    return items;
  }

  @Get(':id')
  getToDo(@Param('id') itemId: string) {
    return this.toDoService.getSingleToDo(itemId);
  }

  @Patch(':id')
  async updateProduct(
    @Param('id') itemId: string,
    @Body('title') itemTitle: string,
    @Body('description') itemDesc: string,
    @Body('type') itemType: number,
  ) {
    await this.toDoService.updateToDo(itemId, itemTitle, itemDesc, itemType);
    return null;
  }
}
