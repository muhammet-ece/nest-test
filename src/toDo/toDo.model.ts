import * as mongoose from 'mongoose';

export const ToDoSchema = new mongoose.Schema({
  title: { type: String, required: true },
  description: { type: String, required: true },
  type: { type: Number, required: true },
});

export interface ToDo extends mongoose.Document {
  id: string;
  title: string;
  description: string;
  type: number;
}
