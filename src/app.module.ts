import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';

import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ToDoModule } from './toDo/toDo.module';

@Module({
  imports: [
    ToDoModule,
    MongooseModule.forRoot(
      'mongodb://localhost:27017/nestjs',
    ),],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
